package com.example.gemavelezvelizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    EditText txtamie,txtgama,txtmarca,txtmodelo,txtcompañia,texto;
    Button btnguardar,btnmodificar,btneliminar,btnconsultageneral, btnconsultaindividual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtamie=findViewById(R.id.IMEI);
        txtgama=findViewById(R.id.Gama);
        txtmarca =findViewById(R.id.Marca);
        txtmodelo=findViewById(R.id.Modelo);
        txtcompañia=findViewById(R.id.compañia);
        btnguardar=findViewById(R.id.btnguardar);//guardar
        btneliminar=findViewById(R.id.btneliminar);
        btnmodificar=findViewById(R.id.btnmmodificar);
        btnconsultageneral=findViewById(R.id.btnconsultageneral);
        btnconsultaindividual=findViewById(R.id.btnconsutaindividul);
        texto=findViewById(R.id.texto);

        btnconsultaindividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               telefono telefon = telefono.findById(telefono.class,
                        Long.parseLong(texto.getText().toString()));
                txtamie.setText(telefon.getAMIE());
                txtgama.setText(telefon.getGama());
                txtmarca.setText(telefon.getMarca());
                txtmodelo.setText(telefon.getModelo());
                txtcompañia.setText(telefon.getCompañia());
            }
        });


        btnconsultageneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inten =new Intent(MainActivity.this,Main2Activity.class);
                startActivity(inten);

            }
        });

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                telefono tell = new telefono(
                        txtamie.getText().toString(),
                        txtgama.getText().toString(),
                        txtmarca.getText().toString(),
                        txtmodelo.getText().toString(),
                        txtcompañia.getText().toString());
                tell.save();


            }
        });
        btnmodificar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                telefono book = telefono.findById(telefono.class, Long.parseLong("1"));
                book.save();

            }
        });
        btneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<telefono> books = telefono.listAll(telefono.class);
                telefono.deleteAll(telefono.class);
            }
        });




    }
}
