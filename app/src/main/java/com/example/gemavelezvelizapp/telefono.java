package com.example.gemavelezvelizapp;

import com.orm.SugarRecord;

public class telefono extends SugarRecord<telefono> {

String AMIE , gama,marca,modelo, compañia;

    public telefono() {
    }

    public telefono(String AMIE, String gama, String marca, String modelo, String compañia) {

        this.AMIE = AMIE;
        this.gama = gama;
        this.marca = marca;
        this.modelo = modelo;
        this.compañia = compañia;
    }

    public String getAMIE() {
        return AMIE;
    }

    public void setAMIE(String AMIE) {
        this.AMIE = AMIE;
    }

    public String getGama() {
        return gama;
    }

    public void setGama(String gama) {
        this.gama = gama;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }
}
